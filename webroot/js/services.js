/**
 * Little JQuery class that turns a button into a service trigger
 * 
 * @param {type} $
 * @returns {undefined}
 */
(function ($) {

    $.fn.service = function (user_options = {}) {

        // Default options
        var options = {
            id: false,
            output: false,
            dry: false
        };
        $.extend(options, user_options);

        // Loop over selected items
        $(this).each(function (key, item) {
            
            var button = $(item);
            
            button.click(function () {

                if (options.output) {
                    $(options.output).text("Loading...");
                }

                button.prop("disabled", true);
                button.removeClass().addClass(["btn", "btn-info"]);
                
                id = options.id ? options.id : button.data("id");
                
                var action = options.dry ? "services/test/" : "services/ping/";

                $.ajax({
                    dataType: "json",
                    url: webroot +  action + id + ".json",
                    success: function (data) {

                        button_class = data.success ? "btn-success" : "btn-danger";
                        button.removeClass().addClass(["btn", button_class]);

                        if (options.output) {
                            $(options.output).text(data.info);
                        }

                        button.prop("disabled", false);
                    },
                    error: function () {
                        if (options.output) {
                            $(options.output).text("Something went wrong");
                        }

                        button.prop("disabled", false);
                    }
                });
            });


        });
    };

}(jQuery));