<?php

declare(strict_types=1);

namespace App\Test\TestCase\Command;

use App\Command\PingCommand;
use Cake\TestSuite\ConsoleIntegrationTestTrait;
use Cake\TestSuite\EmailTrait;
use Cake\TestSuite\TestCase;
use Cake\I18n\FrozenTime;

/**
 * App\Command\PingCommand Test Case
 */
class PingCommandTest extends TestCase
{

    use ConsoleIntegrationTestTrait;
    use EmailTrait;
    
    public $fixtures = [
        'app.Users',
        'app.Hosts',
        'app.Services',
        'app.Pings'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        
        $this->useCommandRunner();
        
        FrozenTime::setTestNow('2020-12-12 16:00:00'); // Put in fake current datetime to make ping
        // time comparisons predictable
    }
    
    public function tearDown(): void
    {
        parent::tearDown();
        
        FrozenTime::setTestNow(); // Restore real datetime
    }

    public function testDescriptionOutput()
    {
        $this->exec('ping --help');
        $this->assertOutputContains('ping all services');
    }

    /**
     * Test execute method
     *
     * @return void
     */
    public function testExecute(): void
    {
        //$this->exec('ping');
        //$this->assertOutputContains('Pinged');
        
        $this->markTestIncomplete('Need to create mock for runners');
    }

}
