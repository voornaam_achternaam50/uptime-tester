<?php

declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\TokensTable;

/**
 * App\Model\Table\TokensTable Test Case
 * 
 * @property TokensTable $Tokens
 */
class TokensTableTest extends TableTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.Tokens',
        'app.Users',
    ];

    // Rely on parent class tests

}
