<?php

declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\PingsTable;

/**
 * App\Model\Table\PingsTable Test Case
 * 
 * @property PingsTable $Pings
 */
class PingsTableTest extends TableTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.Pings',
        'app.Services',
    ];

    // Rely on parent class tests
}
