<?php

namespace App\Test\TestCase\Model\Table;

use Cake\TestSuite\TestCase;
use Cake\Validation\Validator;
use Cake\ORM\RulesChecker;

/**
 * Very basic tests for tables - To be extended from this class
 * 
 * These tests are very superficial and will really only prevent syntax errors.
 */
abstract class TableTestCase extends TestCase
{

    /**
     * Alias of this table (found automatically from testcase name)
     * 
     * @var string
     */
    protected $alias = null;

    /**
     * The specific table object
     * 
     * @var Cake\ORM\Table
     */
    protected $model = null;

    /**
     * Number of validators for a GreaterThanOrEqual assert
     * 
     * @var int
     */
    protected $number_validator = 1;

    /**
     * Run once at the start
     */
    static function setUpBeforeClass(): void
    {

    }

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        
        if (empty($this->alias))
        {
            $class_names = namespaceSplit(static::class);
            $this->alias = substr(end($class_names), 0, -9);
        }
        
        $alias = $this->alias;

        $this->model = $this->getTableLocator()->get($alias);

        $this->{$alias} = $this->model; // Also create a conveniently named reference
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->model);
        unset($this->{$this->alias});

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->assertEquals($this->alias, $this->model->getAlias());
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        // Just call the function to make sure there are no errors
        $validator = $this->model->validationDefault(new Validator());

        // Make sure there are validation fields
        $this->assertGreaterThanOrEqual(
                $this->number_validator,
                count($validator->getIterator())
        );
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        // Just call the function to make sure there are no errors
        $rules = $this->model->buildRules(new RulesChecker());

        $this->assertInstanceOf('Cake\ORM\RulesChecker', $rules);
    }

}
