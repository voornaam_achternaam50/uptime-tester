<?php

declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\HostsTable;

/**
 * App\Model\Table\HostsTable Test Case
 * 
 * @property HostsTable $Hosts
 */
class HostsTableTest extends TableTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.Hosts',
        'app.Services',
    ];

    // Rely on tests of parent class
}
