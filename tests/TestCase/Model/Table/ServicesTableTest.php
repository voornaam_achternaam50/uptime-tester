<?php

declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ServicesTable;

/**
 * App\Model\Table\ServicesTable Test Case
 * 
 * @property ServicesTable $Services
 */
class ServicesTableTest extends TableTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.Services',
        'app.Hosts',
        'app.Pings',
    ];

    // Rely on parent class tests
}
