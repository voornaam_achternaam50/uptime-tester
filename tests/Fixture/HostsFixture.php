<?php

declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * HostsFixture
 */
class HostsFixture extends TestFixture
{

    /**
     * Import existing table (which in turn is based on Migrations)
     * 
     * @var array
     */
    public $import = ['table' => 'hosts'];

    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'id' => 1,
                'user_id' => 1, // Robert
                'name' => 'Roberts Host',
                'description' => 'Hosts belonging to Robert with a bunch of services',
                'domain' => 'robert-roos.nl',
                'created' => '2020-10-25 10:36:44',
                'modified' => '2020-10-25 10:36:44',
            ],
        ];
        parent::init();
    }

}
