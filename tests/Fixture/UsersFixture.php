<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;
use App\Model\Table\UsersTable;

/**
 * UsersFixture
 */
class UsersFixture extends TestFixture
{
    /**
     * Import existing table (which in turn is based on Migrations)
     * 
     * @var array
     */
    public $import = ['table' => 'users'];
    
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'id' => 1,
                'email' => 'robert.soor@gmail.com',
                'name' => 'Robert',
                'password' => '$2y$12$E4YjsPkiz33s6KgeFwZf9u.mUZe7r3AfwfdAJIg20Od36loCCGiVe', // "wachtwoord"
                'type' => UsersTable::TYPE_ADMIN,
                'created' => '2020-11-21 22:22:56',
                'modified' => '2020-11-21 22:22:56'
            ],
        ];
        parent::init();
    }
}
