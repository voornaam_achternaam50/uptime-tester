<?php

declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * TokensFixture
 */
class TokensFixture extends TestFixture
{

    /**
     * Import existing table (which in turn is based on Migrations)
     * 
     * @var array
     */
    public $import = ['table' => 'tokens'];

    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'id' => 1,
                'user_id' => 1,
                'identifier' => 'czzbjxwoxtetehfertio',
                'token' => '$2y$12$/cDiaLz9B/IvpeKNpTiYgu96M2Mxv3BlqNVBj9Om40eDY80Q3MYeG', // "secret"
                'created' => '2020-12-06 17:02:31',
            ],
        ];
        parent::init();
    }

}
