<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * PingsFixture
 */
class PingsFixture extends TestFixture
{
        /**
     * Import existing table (which in turn is based on Migrations)
     * 
     * @var array
     */
    public $import = ['table' => 'pings'];
    
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'id' => 1,
                'service_id' => 1,
                'success' => 1,
                'started' => '2020-11-07 12:27:21',
                'finished' => '2020-11-07 12:27:21',
                'response' => 'This is the ping response.',
                'created' => '2020-11-07 12:27:21',
            ],
        ];
        parent::init();
    }
}
