<?php

declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;
use App\Model\Table\ServicesTable;

/**
 * ServicesFixture
 */
class ServicesFixture extends TestFixture
{

    /**
     * Import existing table (which in turn is based on Migrations)
     * 
     * @var array
     */
    public $import = ['table' => 'services'];

    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'id' => 1,
                'host_id' => 1,
                'name' => 'HTTP service for Roberts server',
                'type' => ServicesTable::TYPE_HTTP,
                'target' => 'https://roberts-website.nl/',
                'rate' => 60, // Every hour
                'created' => '2020-11-01 09:51:26',
                'modified' => '2020-11-01 09:51:26',
            ],
        ];
        parent::init();
    }

}
