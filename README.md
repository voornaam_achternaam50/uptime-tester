# Uptime Tester

[![CodeFactor](https://www.codefactor.io/repository/bitbucket/roberto-r/uptime-tester/badge)](https://www.codefactor.io/repository/bitbucket/roberto-r/uptime-tester)
 
A CakePHP application used to test other services.
 
Built using:  
**CakePHP 4** - [![Build Status](https://img.shields.io/travis/cakephp/app/master.svg?style=flat-square)](https://travis-ci.org/cakephp/app) [![Total Downloads](https://img.shields.io/packagist/dt/cakephp/app.svg?style=flat-square)](https://packagist.org/packages/cakephp/app)
 
## Getting Started
 
### Prerequisites
 
A web server with PHP7 is required, PHP7.2 or higher is recommended.  
Consult the CakePHP documentation for all requirements.
 
### Installing
 
1. Clone repository.
2. Build dependencies by running `composer update -o`, or `composer install -o --no-dev` for production.
3. Create an empty database or acquire credentials to an existing one.
4. Set configuration in the `config/` folder. Copy `app.default.php` to `app.php` and change the settings. The datasource needs to be set at minimum.
4. Visit `/pages/home` in your browser to verify everything is working. Fix issues if they arise.
5. Build the database by running `bin/cake migrations migrate`. Optionally, you can seed the database by running `bin/cake migrations seed`.


### Cronjob

You will need to create a cron job (or something similar) to trigger the scheduled pings. When creating a cron job, it's best to do this as the same user as the webserver to
prevent permission problems in the application cache:

```
sudo -u www-data crontab -e
```

Add the following entry to run the ping command every 15 minutes:
```
*/15 * * * * cd /var/www/uptime && bin/cake ping >> logs/ping.log 2>&1
```
Note the command itself will decide which services need pinging, so the schedule command will often simply do nothing.

## Running the tests
 
Run `# ./vendor/bin/phpunit` to run unit tests. Note that dependencies need to have been built with `--dev` for this to work.  
Specify specif test cases like `# ./vendor/bin/phpunit ./tests/TestCases/Controller/UsersControllerTest.php`. Run only specific tests by added `--filter [regEx test function name]`.
 
## Deployment
 
See the *Installing* section. Mind the use of `--no-dev`.
 
## Migrations and Fixtures
 
Bake the current database with:
```
# bin/cake bake migrations_snapshot Initial
```
 
Write the latest migration to the database:
```
# bin/cake migrations migrate
```
 
Bake a new seed file, based on existing data:
```
# bin/cake bake seed --data Users
```
 
Seed the current database:
```
# bin/cake migrations seed --seed UsersSeed
```
 
Bake fixtures based on the current database:
```
# bin/cake bake fixtures Users --records --count 1000
```
 
 
## Built With
 
* [CakePHP](http://cakephp.com) - CakePHP 4.1
 
## Authors
 
* **Robert Roos** - *Development*
 
