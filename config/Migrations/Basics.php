<?php

declare(strict_types=1);

namespace App\Migrations;

use Migrations\Table;

/**
 * Class with static methods for common fields
 */
class Basics
{

    static public function addCreated(Table $table, array $options = [])
    {
        $options += [
            'default' => null,
            'limit' => null,
            'null' => true,
        ];

        $table->addColumn('created', 'datetime', $options);

        return $table;
    }

    static public function addModified(Table $table, array $options = [])
    {
        $options += [
            'default' => null,
            'limit' => null,
            'null' => true,
        ];

        $table->addColumn('modified', 'datetime', $options);

        return $table;
    }

}
