<?php

declare(strict_types=1);

namespace App\Runner;

use JJG\Ping;

/**
 * Ping service
 */
class RunnerPing extends Runner
{

    /**
     * Start runner
     * 
     * @return bool
     */
    protected function _run() : bool
    {
        $ping = new Ping($this->service->target);
        
        $ping->setTimeout(5); // Seconds
        
        $latency = $ping->ping();
        
        if ($latency !== false)
        {
            $this->response = 'Latency is ' . $latency . ' ms';
        }
        else
        {
            $this->response = 'Host could not be reached';
        }
        
        return $latency !== false;
    }

}
