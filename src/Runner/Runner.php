<?php

declare(strict_types=1);

namespace App\Runner;

use App\Model\Entity\Service;
use Cake\Utility\Text;
use Cake\I18n\FrozenTime;
use DateTimeZone;

/**
 * Base service, to be extended by a specific type
 * 
 * @property Service $service Database service entity
 */
abstract class Runner
{
    
    protected $service = null;
    protected $response = null;
    private $started = null;
    private $finished = null;


    /**
     * Construct service type from service entity
     */
    public function __construct(Service $service)
    {
        $this->service = $service;
    }
    
    /**
     * Run service, specific runners should override _run() instead
     * 
     * @return bool
     */
    public function run() : bool
    {   
        $this->started = new FrozenTime(); // Clock the response
        
        $result = $this->_run();
        
        $this->finished = new FrozenTime();
        
        return $result;
    }
    
    /**
     * Actual execution of the service, to be overridden by a type
     */
    protected abstract function _run() : bool;
    
    /**
     * Return last response
     * 
     * @param int|false $max_length Truncate text length
     * @return null|string
     */
    public function getResponse($max_length = 100)
    {
        if ($max_length && is_string($this->response))
        {
            return Text::truncate($this->response, $max_length);
        }
        
        return $this->response;
    }
    
    /**
     * Get start datetime of last run
     * 
     * @return Time
     */
    public function getStarted()
    {
        return $this->started;
    }
    
    /**
     * Get end datetime of last run
     * 
     * @return Time
     */
    public function getFinished()
    {
        return $this->finished;
    }
}
