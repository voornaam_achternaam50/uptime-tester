<?php

declare(strict_types=1);

namespace App\Runner;

use App\Model\Table\ServicesTable;
use App\Model\Entity\Service;

/**
 * Factory to create runnable services
 */
class RunnerFactory
{

    /**
     * Constructor
     */
    public static function create(Service $service)
    {
        switch ($service->type)
        {
            case ServicesTable::TYPE_HTTP:
                return new RunnerHttp($service);
            case ServicesTable::TYPE_PING:
                return new RunnerPing($service);
            default:
                user_error("No support for type '{$service->type}'");
        }
    }

}
