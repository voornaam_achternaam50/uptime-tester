<?php

declare(strict_types=1);

namespace App\Runner;

use Cake\Http\Client;

/**
 * HTTP(S) service
 */
class RunnerHttp extends Runner
{

    /**
     * Run HTTP tets
     * 
     * Currently, only a 2xx code is registered as succesful.
     * 
     * @return bool
     */
    protected function _run() : bool
    {
        $http = new Client();
        
        $response = $http->get($this->service->target);
        
        $this->response = (string)$response->getBody()->getContents();
        
        $code = $response->getStatusCode();
        
        return ($code >= 200 && $code < 300);
    }

}
