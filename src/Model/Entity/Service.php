<?php

declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;
use App\Model\Table\ServicesTable;

/**
 * Service Entity
 *
 * @property int $id
 * @property int $host_id
 * @property string $name
 * @property string $type
 * @property string $target
 * @property int|null $rate
 * @property \Cake\I18n\FrozenTime|null $created
 * @property \Cake\I18n\FrozenTime|null $modified
 * @property string $typeLabel
 * @property float $uptimeFromTotal
 * @property string $intervalFormat
 *
 * @property \App\Model\Entity\Host $host
 * @property \App\Model\Entity\Ping[] $pings
 */
class Service extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'host_id' => true,
        'name' => true,
        'type' => true,
        'target' => true,
        'rate' => true,
        'created' => true,
        'modified' => true,
        'host' => true,
        'pings' => true,
    ];

    /**
     * Get pretty name of type
     * 
     * @return string
     */
    protected function _getTypeLabel()
    {
        $type = $this->type;
        return ServicesTable::TYPES_LABELS[$type];
    }
    
    /**
     * Get uptime ratio from downtime and uptime fields
     * 
     * @return real
     */
    protected function _getUptimeFromTotal()
    {
        if (is_null($this->time_total) || is_null($this->uptime))
        {
            return null;
        }
        
        $total = floatval($this->time_total);
        $up = floatval($this->uptime);
        
        if ($total < 1)
        {
            return null;
        }
        
        $ratio = $up / ($total);
        
        return $ratio;
    }
    
    /**
     * Get human-readable format of rate
     * 
     * \DateInterval is not helpful here.
     * 
     * @return string
     */
    protected function _getIntervalFormat()
    {
        if (is_null($this->rate))
        {
            return 'Never';
        }
        
        if ($this->rate < 60)
        {
            return "{$this->rate} minutes";
        }
        elseif ($this->rate < 1440)
        {
            $hours = abs($this->rate / 60);
            return "{$hours} hour(s)";
        }
        else
        {
            $days = abs($this->rate / 60 / 24);
            return "{$days} day(s)";
        }
    }
}
