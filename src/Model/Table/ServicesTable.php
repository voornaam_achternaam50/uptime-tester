<?php

declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\I18n\FrozenTime;

/**
 * Services Model
 *
 * @property \App\Model\Table\HostsTable&\Cake\ORM\Association\BelongsTo $Hosts
 * @property \App\Model\Table\PingsTable&\Cake\ORM\Association\HasMany $Pings
 *
 * @method \App\Model\Entity\Service newEmptyEntity()
 * @method \App\Model\Entity\Service newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\Service[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Service get($primaryKey, $options = [])
 * @method \App\Model\Entity\Service findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\Service patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Service[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\Service|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Service saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Service[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Service[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\Service[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Service[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class ServicesTable extends Table
{

    /**
     * Service types
     */
    const TYPE_PING = 'ping';
    const TYPE_HTTP = 'http';

    /** List of service types */
    const TYPES = [self::TYPE_PING, self::TYPE_HTTP];

    /** Serivce types and human-readable names */
    const TYPES_LABELS = [
        self::TYPE_PING => 'Ping',
        self::TYPE_HTTP => 'HTTP(S)'
    ];

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('services');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Hosts');
        $this->hasMany('Pings')
                ->setDependent(true); // Cascading delete

        $this->hasOne('LastPings')
                ->setClassName('Pings')
                ->setStrategy('select')
                ->setFinder('mostRecent');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
                ->integer('id')
                ->allowEmptyString('id', null, 'create');

        $validator
                ->scalar('name')
                ->maxLength('name', 100)
                ->requirePresence('name', 'create')
                ->notEmptyString('name');

        $validator
                ->scalar('type')
                ->inList('type', self::TYPES)
                ->requirePresence('type', 'create')
                ->notEmptyString('type');

        $validator
                ->scalar('target')
                ->maxLength('target', 255)
                ->requirePresence('target', 'create')
                ->notEmptyString('target');

        $validator
                ->integer('rate')
                ->greaterThan('reate', 4, 'Interval must be at least 5 minutes')
                ->allowEmptyString('rate');

        return $validator;
    }

    /**
     * Validation specific for ping type
     * 
     * @param Validator $validator
     * @return Validator
     */
    public function validationPing(Validator $validator): Validator
    {
        $validator->regex('target', '/^[a-z0-9\.\-]*$/', 'Not a valid domain or IP address');

        return $validator;
    }

    /**
     * Validation specific for http type
     * 
     * @param Validator $validator
     * @return Validator
     */
    public function validationHttp(Validator $validator): Validator
    {
        $validator->url('target', 'Not a valid URL');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['host_id'], 'Hosts'), ['errorField' => 'host_id']);

        // Add rule for type specific validation
        $rules->add(function($entity)
        {
            $data = $entity->toArray();
            $validator = $this->getValidator($data['type']);
            $errors = $validator->validate($data, $entity->isNew());
            $entity->setErrors($errors);

            return empty($errors);
        }, 'type_rule');


        return $rules;
    }

    /**
     * Join with pings and count an average uptime of a specified period
     * 
     * The uptime is based on counts of succesful pings, not on time!
     * By default this is over the past month.
     * $options supports the `from` and `to` datetimes for the computaton. By 
     * default, `to` is emtpy and `from` is one month ago.
     * 
     * @param Query $query
     * @param array $options
     * @return Query
     */
    public function findUptimeCount(Query $query, array $options): Query
    {
        $options += ['from' => new FrozenTime('-1 month'), 'to' => false];

        return $query
                        ->select($this)
                        ->select(['uptime' => 'AVG(Pings.success)'])
                        ->leftJoinWith('Pings', function ($q) use ($options)
                        {
                            if ($options['from'])
                            {
                                $q->where(['Pings.finished >=' => $options['from']]);
                            }

                            if ($options['to'])
                            {
                                $q->where(['Pings.finished <' => $options['to']]);
                            }

                            return $q;
                        })
                        ->group('Services.id');
    }

    /**
     * Join with pings and compute the uptime
     * 
     * Adds the `uptime` and `total_time` fields (both in seconds).
     * By default this is over the past month.
     * $options supports the `from` and `to` datetimes for the computaton. By 
     * default, `to` is emtpy and `from` is one month ago.
     * 
     * It seems impossible to join with Pings->findWithPeriod, so instead we
     * perform two manual joints in this finder, twice to Pings.
     * 
     * @param Query $query
     * @param array $options
     * @return Query
     */
    public function findUpAndDowntime(Query $query, array $options): Query
    {
        $options += ['from' => new FrozenTime('-1 month'), 'to' => false];
        
        $time = 'TIME_TO_SEC(TIMEDIFF(p2.finished, Pings.finished))';
        $uptime = "SUM(CASE WHEN Pings.success > 0 THEN {$time} ELSE 0 END)";
        $time_total = "SUM({$time})";

        $subquery = 'SELECT MIN(p3.finished) FROM pings p3 WHERE '
                . 'p3.finished > Pings.finished AND '
                . 'p3.service_id = Pings.service_id';

        $p1_join_on = [
            'Services.id = Pings.service_id'
        ];
        $p2_join_on = [
            'Pings.service_id = p2.service_id',
            "p2.finished = ({$subquery})"
        ];

        if ($options['from'])
        {
            $p1_join_on['Pings.finished >='] = $options['from'];
        }
        if ($options['to'])
        {
            $p1_join_on['Pings.finished <'] = $options['to'];
        }

        $query
                ->select($this)
                ->select([
                    'time_total' => $time_total,
                    'uptime' => $uptime
                ])
                ->leftJoin(['Pings' => 'pings'], $p1_join_on, ['Pings.finished' => 'datetime'])
                ->leftJoin(['p2' => 'pings'], $p2_join_on)
                ->group('Services.id');

        return $query;
    }
    
    /**
     * Find services where it is time for an automated ping
     * 
     * @param Query $query
     * @param array $options
     * @return Query
     */
    public function findNeededPings(Query $query, array $options)
    {
        // Use PHP time instead of SQL `NOW()` to conistently use the server
        // time zone, whatever it might be
        $now = new FrozenTime();

        $diff = $query->func()->timestampdiff([
            'MINUTE' => 'literal',
            $query->func()->max('Pings.finished'),
            $now->format("'Y-m-d H:i:s'") => 'literal'
        ]); // `TIMESTAMPDIFF(MINUTE, MAX(Pings.finished), NOW())`

        return $query
                ->select($this)
                ->select(['time_past' => $diff])
                ->where(['Services.rate is NOT NULL']) // Only when rate is set
                ->leftJoinWith('Pings')
                ->group('Services.id')
                ->having([
                    'OR' => [
                        ['time_past is NULL'], // No pings yet, or...
                        ['time_past >= Services.rate'] // Last ping long ago
                    ]
        ]);
    }

}
