<?php

declare(strict_types=1);

namespace App\Policy;

use App\Model\Table\UsersTable;
use Authorization\IdentityInterface;
use Cake\ORM\Query;

/**
 * Users policy
 */
class UsersTablePolicy
{

    /**
     * Check if user can index
     */
    public function canIndex(IdentityInterface $user)
    {
        return $user->getOriginalData()->type == UsersTable::TYPE_ADMIN;
    }

}
