<?php

declare(strict_types=1);

namespace App\Policy;

use App\Model\Entity\Service;
use Authorization\IdentityInterface;
use Cake\ORM\TableRegistry;

/**
 * Service policy
 */
class ServicePolicy
{

    /**
     * Check if $user can create Service
     *
     * @param Authorization\IdentityInterface $user The user.
     * @param App\Model\Entity\Service $service
     * @return bool
     */
    public function canCreate(IdentityInterface $user, Service $service)
    {
        return true; // Everyone can created
    }

    /**
     * Check if $user can update Service
     *
     * @param Authorization\IdentityInterface $user The user.
     * @param App\Model\Entity\Service $service
     * @return bool
     */
    public function canUpdate(IdentityInterface $user, Service $service)
    {
        return $this->isOwnedBy($user, $service);
    }

    /**
     * Check if $user can delete Service
     *
     * @param Authorization\IdentityInterface $user The user.
     * @param App\Model\Entity\Service $service
     * @return bool
     */
    public function canDelete(IdentityInterface $user, Service $service)
    {
        return $this->canUpdate($user, $service);
    }

    /**
     * Check if $user can view Service
     *
     * @param Authorization\IdentityInterface $user The user.
     * @param App\Model\Entity\Service $service
     * @return bool
     */
    public function canView(IdentityInterface $user, Service $service)
    {
        return $this->canUpdate($user, $service);
    }
    
    /**
     * Check if user owns the host that belongs to this service
     * 
     * @param IdentityInterface $user
     * @param \App\Policy\Host $service
     * @return bool
     */
    private function isOwnedBy(IdentityInterface $user, Service $service)
    {
        if ($service->has('host'))
        {
            $host = $service->host;  // Host was already contained
        }
        else
        {
            $hostsTable = TableRegistry::getTableLocator()->get('Hosts');
            $host = $hostsTable->get($service->host_id);
        }
        
        return $host->user_id === $user->id;
    }

}
