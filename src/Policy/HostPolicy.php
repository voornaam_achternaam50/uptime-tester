<?php

declare(strict_types=1);

namespace App\Policy;

use App\Model\Entity\Host;
use Authorization\IdentityInterface;

/**
 * Host policy
 */
class HostPolicy
{

    /**
     * Check if $user can create Host
     *
     * @param Authorization\IdentityInterface $user The user.
     * @param App\Model\Entity\Host $host
     * @return bool
     */
    public function canCreate(IdentityInterface $user, Host $host)
    {
        return true; // Everyone can created
    }

    /**
     * Check if $user can update Host
     *
     * @param Authorization\IdentityInterface $user The user.
     * @param App\Model\Entity\Host $host
     * @return bool
     */
    public function canUpdate(IdentityInterface $user, Host $host)
    {
        return $this->isOwner($user, $host); // Only owner can edit
    }

    /**
     * Check if $user can delete Host
     *
     * @param Authorization\IdentityInterface $user The user.
     * @param App\Model\Entity\Host $host
     * @return bool
     */
    public function canDelete(IdentityInterface $user, Host $host)
    {
        return $this->canUpdate($user, $host);
    }

    /**
     * Check if $user can view Host
     *
     * @param Authorization\IdentityInterface $user The user.
     * @param App\Model\Entity\Host $host
     * @return bool
     */
    public function canView(IdentityInterface $user, Host $host)
    {
        return $this->canUpdate($user, $host); // Only owner can view
    }
    
    /**
     * Check if user owns this Host
     * 
     * @param IdentityInterface $user
     * @param Host $host
     * @return bool
     */
    private function isOwner(IdentityInterface $user, Host $host)
    {
        return $user->id === $host->user_id;
    }

}
