<?php
declare(strict_types=1);

namespace App\Policy;

use App\Model\Table\HostsTable;
use Authorization\IdentityInterface;
use Cake\ORM\Query;

/**
 * Hosts policy
 */
class HostsTablePolicy
{
    
    /**
     * Limit users to their own hosts
     * 
     * @param IdentityInterface $user
     * @param Query $query
     * @return Query
     */
    public function scopeIndex(IdentityInterface $user, Query $query)
    {
        return $query->where(['Hosts.user_id' => $user->getIdentifier()]);
    }

}
