<?php

declare(strict_types=1);

namespace App\Controller;

/**
 * Hosts Controller
 *
 * @property \App\Model\Table\HostsTable $Hosts
 * @method \App\Model\Entity\Host[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class HostsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        $this->Authorization->skipAuthorization();
        
        $query = $this->Hosts->find();
        
        $query = $this->Authorization->applyScope($query, 'index');
        
        $hosts = $this->paginate($query);

        $this->set(compact('hosts'));
    }

    /**
     * View method
     *
     * @param string|null $id Host id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $host = $this->Hosts->get($id, [
            'contain' => [
                'Services' => function ($q)
                {
                    return $q->find('upAndDowntime');
                },
                'Services.LastPings',
            ]
        ]);

        $this->Authorization->authorize($host, 'view');

        $this->set(compact('host'));
    }

    /**
     * Edit and add method
     *
     * @param string|null $id Host id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        if (is_null($id))
        {
            $host = $this->Hosts->newEmptyEntity();

            $this->Authorization->authorize($host, 'create');
        }
        else
        {
            $host = $this->Hosts->get($id);
            $this->Authorization->authorize($host, 'update');
        }

        if ($this->request->is(['patch', 'post', 'put']))
        {
            $host->user_id = $this->request->getAttribute('identity')->getIdentifier();
            $host = $this->Hosts->patchEntity($host, $this->request->getData());
            
            if ($this->Hosts->save($host))
            {
                $this->Flash->success(__('The host has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The host could not be saved. Please, try again.'));
        }
        $this->set(compact('host'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Host id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $host = $this->Hosts->get($id);
        
        $this->Authorization->authorize($host, 'delete');
        
        if ($this->Hosts->delete($host))
        {
            $this->Flash->success(__('The host has been deleted.'));
        }
        else
        {
            $this->Flash->error(__('The host could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

}
