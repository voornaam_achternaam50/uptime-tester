<?php

declare(strict_types=1);

namespace App\Controller;

use Cake\Event\EventInterface;
use App\Model\Table\UsersTable;
use App\Model\Entity\User;
use App\Model\Entity\Token;
use Cake\Mailer\Mailer;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class UsersController extends AppController
{

    /**
     * beforeFilter
     * 
     * @param EventInterface $event
     */
    public function beforeFilter(EventInterface $event)
    {
        parent::beforeFilter($event);

        $this->Authentication->allowUnauthenticated([
            'login', 'register', 'logout', 'activate', 'forgotPassword', 'recover'
        ]);

        if ($this->Authentication->getIdentity())
        {
            $this->Authorization->authorizeModel('index');
            $this->Authorization->authorizeAction();
        }
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        $this->paginate = [
            'order' => ['created' => 'desc']
        ];

        $query = $this->Users->find('countServices');

        $users = $this->paginate($query);

        $this->set(compact('users'));
    }

    /**
     * Login method
     */
    public function login()
    {
        $this->Authorization->skipAuthorization(); // Public

        $result = $this->Authentication->getResult();

        // If the user is logged in send them away.
        if ($result->isValid())
        {
            $target = $this->Authentication->getLoginRedirect() ?? '/';
            return $this->redirect($target);
        }
        if ($this->request->is('post'))
        {
            if (!$result->isValid())
            {
                $this->Flash->error('Invalid username or password - Has your account been activated yet?');
            }
        }
        elseif ($this->request->is('get'))
        {
            $this->request = $this->request->withData('remember_me', true); // Default checkbox value
        }
    }

    /**
     * Logout method
     */
    public function logout()
    {
        $this->Authorization->skipAuthorization(); // Public

        $this->Authentication->logout();
        return $this->redirect(['controller' => 'Users', 'action' => 'login']);
    }

    /**
     * View method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => ['Hosts'],
        ]);

        $this->set(compact('user'));
    }

    /**
     * Register oneself
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function register()
    {
        $this->Authorization->skipAuthorization(); // Public

        $user = $this->Users->newEmptyEntity();

        if ($this->request->is('post'))
        {
            $data = $this->request->getData();

            $user = $this->Users->patchEntity($user, $data);
            if ($this->Users->save($user))
            {
                $token = $this->Users->Tokens->createAndSaveToken($user->id);
                $this->_sendRecoveryEmail(
                        $user, $token, 'Completing account registration', 'user_register'
                );

                $this->Flash->success('Your account has been created - Follow the link in the email send to complete the registration');

                return $this->redirect(['controller' => 'Users', 'action' => 'login']);
            }
            $this->Flash->error('Account could not be created');
        }

        $this->set(compact('user'));
    }

    /**
     * Edit method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put']))
        {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            if ($this->Users->save($user))
            {
                $this->Flash->success(__('The user has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The user could not be saved. Please, try again.'));
        }
        $this->set(compact('user'));
    }

    /**
     * Delete method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $user = $this->Users->get($id);

        $this->Authorization->authorize($user, 'delete');

        if ($this->Users->delete($user))
        {
            $this->Flash->success(__('The user has been deleted.'));
        }
        else
        {
            $this->Flash->error(__('The user could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    /**
     * Activate account
     * 
     * @param string $identifier
     * @param string $secret
     */
    public function activate($identifier = null, $secret = null)
    {
        $this->Authorization->skipAuthorization(); // Public

        if ($this->Authentication->getIdentity())
        {
            $this->Flash->success('Already logged in');
            return $this->redirect('/');
        }

        $user = $this->Users->Tokens->validate($identifier, $secret);

        if ($user)
        {
            if (is_null($user->type))
            {
                $this->Users->patchEntity($user, ['type' => UsersTable::TYPE_USER]);
                if ($this->Users->save($user))
                {
                    $this->Flash->success('Your account has been activated');
                    $this->Users->Tokens->delete($user->token);
                }
                else
                {
                    $this->Flash->success('Failed to activate account, please try again');
                }
            }
        }
        else
        {
            $this->Flash->error('Could not verify link - Try resetting your password to complete the account activation');
        }

        return $this->redirect(['action' => 'login']);
    }

    /**
     * Send email with password recovery
     */
    public function forgotPassword()
    {
        $this->Authorization->skipAuthorization();

        if ($this->Authentication->getIdentity())
        {
            $this->Flash->success('Already logged in');
            return $this->redirect('/');
        }

        if ($this->request->is(['post', 'put']))
        {
            $email = $this->request->getData('email');

            $user = $this->Users->find()->where(compact('email'))
                    ->first();

            if ($user)
            {
                $token = $this->Users->Tokens->createAndSaveToken($user->id);
                $this->_sendRecoveryEmail(
                        $user, $token, 'Reset your password', 'user_recover'
                );
            }

            $this->Flash->info('A recovery email has been sent to the specified email');
        }
    }

    /**
     * Reset user password
     * 
     * @param string $identifier
     * @param string $secret
     */
    public function recover($identifier = null, $secret = null)
    {
        $this->Authorization->skipAuthorization();

        if ($this->Authentication->getIdentity())
        {
            $this->Flash->success('Already logged in');
            return $this->redirect('/');
        }

        $user = $this->Users->Tokens->validate($identifier, $secret);

        if (!$user)
        {
            $this->Flash->error('Invalid link');
            return $this->redirect(['action' => 'login']);
        }

        if ($this->request->is(['post', 'patch', 'put']))
        {
            $this->Users->patchEntity($user, $this->request->getData(), [
                'fields' => 'password'
            ]);

            if (is_null($user->type))
            {
                $user->type = UsersTable::TYPE_USER; // Also activate if needed
            }

            if ($this->Users->save($user))
            {
                $this->Users->Tokens->delete($user->token);
                $this->Flash->success('Password has been updated');
                return $this->redirect(['action' => 'login']);
            }
            $this->Flash->error('Failed to update password, try again');
        }
        else
        {
            $this->request = $this->request->withData('password', ''); // Overwrite existing password
        }

        $this->set(compact('user'));
    }

    /**
     * Send recovery email
     * 
     * Set $action to an email template. Any template for a user and token is 
     * appropiate.
     * 
     * @param User $user
     * @param Token $token
     * @param string $subject Email subject
     * @param string $template
     */
    protected function _sendRecoveryEmail(User $user, Token $token, string $subject, string $template)
    {
        $mailer = new Mailer();
        $mailer->setSubject($subject)
                ->setTo($user->email)
                ->setViewVars(compact('user', 'token'))
                ->setEmailFormat('html');
        $mailer->viewBuilder()->setTemplate($template);

        return $mailer->send();
    }

}
