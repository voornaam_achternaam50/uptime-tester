<?php

use App\Model\Table\ServicesTable;

/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Service $service
 */
?>

<div class="form content">
    <?= $this->Form->create($service) ?>
    <fieldset>
        <legend><?= $service->isNew() ? 'Add Service' : 'Edit Service' ?></legend>
        <?php
        echo $this->Form->control('host_id', ['options' => $hosts]);
        echo $this->Form->control('name');
        echo $this->Form->control('type', ['type' => 'select', 'options' => ServicesTable::TYPES_LABELS]);
        echo $this->Form->control('target');
        echo $this->Form->control('rate', [
            'label' => 'Interval',
            'type' => 'select',
            'options' => [15 => '15 minutes', 60 => '1 hour', 360 => '6 hours', 1440 => '1 day'],
            'empty' => 'Never'
        ]);
        ?>
    </fieldset>
    <div class="btn-group" role="group">
        <?= $this->Form->button('Submit') ?>
        <?= $this->Html->link('Cancel', ['controller' => 'Hosts', 'action' => 'view', $service->host_id], ['class' => 'btn btn-primary']) ?>
    </div>
    <?= $this->Form->end() ?>
</div>
