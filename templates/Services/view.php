<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Service $service
 */
$this->Html->script('services', ['block' => true]);
?>

<div class="content">
    <h3>Service: <?= h($service->name) ?></h3>
    <table class="table table-sm">
        <tr>
            <th><?= __('Host') ?></th>
            <td><?= $this->Html->link($service->host->name, ['controller' => 'Hosts', 'action' => 'view', $service->host->id]) ?></td>
        </tr>
        <tr>
            <th><?= __('Name') ?></th>
            <td><?= h($service->name) ?></td>
        </tr>
        <tr>
            <th><?= __('Type') ?></th>
            <td><?= h($service->typeLabel) ?></td>
        </tr>
        <tr>
            <th><?= __('Target') ?></th>
            <td><?= h($service->target) ?></td>
        </tr>
    </table>

    <?= $this->Html->link('Back to host', ['controller' => 'Hosts', 'action' => 'view', $service->host_id], ['class' => 'btn btn-secondary']) ?>
    <br><br>
    <?= $this->Html->link('Edit', ['action' => 'edit', $service->id], ['class' => 'btn btn-secondary']) ?>
    <br><br>
    <?= $this->Form->button('Test', ['type' => 'button', 'id' => 'test-service', 'class' => 'btn-primary']) ?>

    <div id="test-result"></div>

    <div class="related">

        <h4><?= __('Pings') ?></h4>
        <?php if (!empty($service->pings)) : ?>
            <div class="table-responsive">
                <table class="table table-sm">
                    <tr>
                        <th><?= $this->Paginator->sort('success') ?></th>
                        <th><?=
                            $this->Paginator->sort(
                                    'finished', 'Time', ['direction' => 'desc']
                            )
                            ?></th>
                    </tr>
                    <?php foreach ($service->pings as $pings) : ?>
                        <tr>
                            <td>
                                <?php if ($pings->success) : ?>
                                    <span class="badge badge-pill badge-success">Online</span>
                                <?php else: ?>
                                    <span class="badge badge-pill badge-warning">Failed</span>
                                <?php endif; ?>
                            </td>
                            <td><?= h($pings->finished) ?></td>
                        </tr>
                    <?php endforeach; ?>
                </table>
            </div>
        <?php endif; ?>

        <?= $this->element('basics/pagination_panel') ?>

    </div>
</div>

<script>
    $(document).ready(function () {

        $("button#test-service").service({
            id: <?= $service->id ?>,
            dry: true,
            output: "div#test-result"
        });
    });
</script>