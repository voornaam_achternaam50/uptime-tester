<?php

$uptime = $service->uptimeFromTotal;

if (!is_null($uptime)) :

    $class = 'badge ';
    if ($uptime >= 1)
    {
        $class .= 'badge-success';
    }
    elseif ($uptime >= 0.5)
    {
        $class .= 'badge-warning';
    }
    else
    {
        $class .= 'badge-danger';
    }
    ?>

    <span class="<?= $class ?>"><?= $this->Number->toPercentage($uptime, 0, ['multiply' => true]) ?></span>

<?php else: ?>
    
    <span class="badge badge-secondary">NaN</span>

<?php endif; ?>
