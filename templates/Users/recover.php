<?php
/* @var $this Cake\View\View */
?>

<div class="form content">
    <?= $this->Form->create($user) ?>
    <fieldset>
        <legend>Reset your password</legend>
        <?= $this->Form->control('password') ?>
        <?= $this->Form->control('password_confirm', ['type' => 'password']) ?>
    </fieldset>
    <?= $this->Form->button('Save') ?>
    <?= $this->Form->end() ?>

    <br>
    <?= $this->Html->link('Back to login', ['action' => 'login']) ?>
</div>
