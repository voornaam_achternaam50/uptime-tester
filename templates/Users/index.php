<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Host[]|\Cake\Collection\CollectionInterface $hosts
 */
?>
<div class="content">

    <h3>Users</h3>

    <div class="table-responsive">
        <table class="table">
            <thead>
                <tr>
                    <th><?= $this->Paginator->sort('email') ?></th>
                    <th><?= $this->Paginator->sort('name') ?></th>
                    <th><?= $this->Paginator->sort('type') ?></th>
                    <th># Hosts / # Number Services </th>
                    <th><?= $this->Paginator->sort('created', null, ['direction' => 'desc']) ?></th>
                    <th class="actions"></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($users as $user): ?>
                    <tr>
                        <td><?= h($user->email) ?></td>
                        <td><?= h($user->name) ?></td>
                        <td><?= h($user->typeFormat) ?></td>
                        <td>
                            <?= $user->count_hosts ?> / 
                            <?= $user->count_services ?>
                        </td>
                        <td><?= $user->created ?></td>
                        <td class="actions">
                            <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $user->id], ['confirm' => __('Are you sure you want to delete "{0}"?', $user->email)]) ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>

    <?= $this->element('basics/pagination_panel') ?>

</div>
