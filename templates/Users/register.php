<div class="form content">
    
    <?= $this->Form->create($user) ?>
    
    <?= $this->Form->control('email') ?>
    <?= $this->Form->control('name') ?>
    <?= $this->Form->control('password') ?>
    <?= $this->Form->control('password_confirm', ['type' => 'password']) ?>
    
    <?= $this->Form->button('Login') ?>
    <?= $this->Form->end() ?>
    
</div>
