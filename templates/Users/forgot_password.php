<?php
/* @var $this Cake\View\View */
?>

<div class="form content">
    <?= $this->Form->create() ?>
    <fieldset>
        <legend>Reset your password</legend>
        <?= $this->Form->control('email', ['required' => true]) ?>
    </fieldset>
    <?= $this->Form->button('Reset Password') ?>
    <?= $this->Form->end() ?>

    <br>
    <?= $this->Html->link('Back to login', ['action' => 'login']) ?>
</div>
