<?php

use Cake\ORM\TableRegistry;

/* @var $this App\View\AppView */
/* @var $user Authorization\Identity */

$user = $this->request->getAttribute('identity');
?>

<!DOCTYPE html>
<html>
    <head>
        <?= $this->Html->charset() ?>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>
            Uptime Tester: <?= $this->fetch('title') ?>
        </title>
        <?= $this->Html->meta('icon') ?>

        <?= $this->Html->css('bootstrap.min') ?>
        <?= $this->Html->script(['jquery.min', 'popper.min', 'bootstrap.min']) ?>

        <?= $this->Html->css(['cake', 'style']) ?>

        <script>
            var webroot = "<?= $this->request->getAttribute('webroot') ?>";
        </script>

        <?= $this->fetch('meta') ?>
        <?= $this->fetch('css') ?>
        <?= $this->fetch('script') ?>
    </head>
    <body>
        <nav class="top-nav">
            <div class="top-nav-title">
                <?= $this->Html->image('uptime.svg', ['width' => '40px']) ?>
                &nbsp;
                <a href="<?= $this->Url->build('/') ?>"><span>Uptime </span>Tester</a>
            </div>
            <div class="top-nav-links">
                <?= $this->Html->link('Hosts', ['controller' => 'Hosts', 'action' => 'index']) ?>

                <?php
                if ($user && $user->can('index', TableRegistry::getTableLocator()->get('Users')))
                {
                    echo $this->Html->link('Users', ['controller' => 'Users', 'action' => 'index']);
                }
                ?>

                <span class="nav-links-alt">
                    <?php
                    if ($user)
                    {
                        echo $user->getOriginalData()->name;
                        echo $this->Html->link('Logout', ['controller' => 'Users', 'action' => 'logout']);
                    }
                    else
                    {
                        echo $this->Html->link('Login', ['controller' => 'Users', 'action' => 'login']);
                    }
                    ?>
                </span>

            </div>
        </nav>
        <main class="main">
            <div class="container">
                <?= $this->Flash->render() ?>
                <?= $this->fetch('content') ?>
            </div>
        </main>
    </body>
</html>
