<?php
/**
 * Email to activate a new account
 */
/* @var $user App\Model\Entity\User */
/* @var $token App\Model\Entity\Token */
/* @var $this Cake\View\View */

$url = $this->Url->build([
    'controller' => 'Users',
    'action' => 'activate',
    $token->identifier,
    $token->secret
        ], ['fullBase' => true]);
?>

<p>Dear <?= $user->name ?>,</p>

<p>
    Click on the link below (or copy it into your web browser) to verify your 
    email and complete the account registration:
</p>

<p>
    <?= $this->Html->link($url, $url) ?>
</p>

<p>
    If you did not create this account, contact the administrator. Your
    credentials are not necessary in danger.
</p>

<p>
    Kind regards,<br>
    The Uptime-Tester<br>
    (This email has been sent automatically)
</p>
