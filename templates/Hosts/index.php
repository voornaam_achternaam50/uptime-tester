<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Host[]|\Cake\Collection\CollectionInterface $hosts
 */
?>
<div class="content">
    
    <?= $this->Html->link(__('New Host'), ['action' => 'edit'], ['class' => 'btn btn-primary float-right']) ?>
    <h3><?= __('Hosts') ?></h3>
    
    <div class="table-responsive">
        <table class="table">
            <thead>
                <tr>
                    <th><?= $this->Paginator->sort('name') ?></th>
                    <th><?= $this->Paginator->sort('domain') ?></th>
                    <th><?= $this->Paginator->sort('created') ?></th>
                    <th class="actions"></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($hosts as $host): ?>
                    <tr>
                        <td><?= $this->Html->link($host->name, ['action' => 'view', $host->id]) ?></td>
                        <td><?= h($host->domain) ?></td>
                        <td><?= h($host->created) ?></td>
                        <td class="actions">
                            <?= $this->Html->link(__('Edit'), ['action' => 'edit', $host->id]) ?>
                            <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $host->id], ['confirm' => __('Are you sure you want to delete "{0}"?', $host->name)]) ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    
    <?= $this->element('basics/pagination_panel') ?>

</div>
