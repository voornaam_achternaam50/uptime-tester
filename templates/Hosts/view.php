<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Host $host
 */
$this->Html->script('services', ['block' => true]);
?>
<div class="content">
    <h3>Host: <?= h($host->name) ?></h3>
    <table class="table table-sm">
        <tr>
            <th>Name</th>
            <td><?= h($host->name) ?></td>
        </tr>
        <tr>
            <th>Domain</th>
            <td><?= h($host->domain) ?></td>
        </tr>
        <tr>
            <th>Created</th>
            <td><?= h($host->created) ?></td>
        </tr>
        <tr>
            <th>Description</th>
            <td>
                <blockquote>
                    <?= $this->Text->autoParagraph(h($host->description)); ?>
                </blockquote>
            </td>
        </tr>
    </table>

    <div class="related">

        <?= $this->Html->link('New Service', ['controller' => 'Services', 'action' => 'edit', '?' => ['host_id' => $host->id]], ['class' => 'btn btn-primary float-right']) ?>
        <h4>Services</h4>

        <?php if (!empty($host->services)) : ?>
            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Last status / Up-time</th>
                            <th>Type</th>
                            <th>Target</th>
                            <th>Interval</th>
                            <th class="actions"></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($host->services as $service) : ?>
                            <tr>
                                <td><?= $this->Html->link($service->name, ['controller' => 'Services', 'action' => 'view', $service->id]) ?></td>
                                <td>
                                    <?= $this->element('service/status', compact('service')) ?>
                                    <?= $this->element('service/uptime', compact('service')) ?><br>
                                    <?php if ($service->has('last_ping')): ?>
                                        <span class="last-ping-date">
                                            Last: <?= $service->last_ping->finished ?>
                                        </span>
                                    <?php endif; ?>
                                </td>
                                <td><?= h($service->typeLabel) ?></td>
                                <td><?= h($service->target) ?></td>
                                <td><?= $service->interval_format ?></td>
                                <td class="actions">
                                    <?=
                                    $this->Form->button('Run', [
                                        'type' => 'button',
                                        'class' => 'btn btn-secondary test-service',
                                        'data-id' => $service->id
                                    ])
                                    ?>
                                    <?= $this->Html->link('Edit', ['controller' => 'Services', 'action' => 'edit', $service->id]) ?>
                                    <?= $this->Form->postLink('Delete', ['controller' => 'Services', 'action' => 'delete', $service->id], ['confirm' => __('Are you sure you want to delete "{0}"?', $service->name)]) ?>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        <?php endif; ?>
    </div>
</div>

<script>
    $(document).ready(function () {

        $("button.test-service").service();
    });
</script>