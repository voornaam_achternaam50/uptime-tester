<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Host $host
 */
?>

<div class="form content">
    <?= $this->Form->create($host) ?>
    <fieldset>
        <legend><?= $host->isNew() ? 'Add Host' : 'Edit Host' ?></legend>
        <?php
        echo $this->Form->control('name');
        echo $this->Form->control('description');
        echo $this->Form->control('domain');
        ?>
    </fieldset>
    <div class="btn-group" role="group">
        <?= $this->Form->button('Submit') ?>
        <?= $this->Html->link('Cancel', ['action' => 'index'], ['class' => 'btn btn-primary']) ?>
    </div>
    <?= $this->Form->end() ?>
</div>
